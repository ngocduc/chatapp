import React, { useState, useEffect } from 'react'
import { GiftedChat } from 'react-native-gifted-chat'
import database from '@react-native-firebase/database';

const Example = () => {
  const [state, setState] = useState({
    messages: [],
  })

  useEffect(() => {
    // setInterval(() => {

    database().ref('/users').push({
      mail: Math.random() * 1000,
    }).then((data) => {
      //success callback
      console.log('data push', data)
    }).catch((error) => {
      //error callback
      console.log('error ', error)
    })

    // }, 2000)

    // database()
    //   .ref('/users')
    //   .once('value')
    //   .then(snapshot => {
    //     console.log('User data: ', snapshot.val());
    //   });
    // database().ref('/users').set({
    //     mail: 'mail',
    // }).then((data)=>{
    //     //success callback
    //     console.log('data ' , data)
    // }).catch((error)=>{
    //     //error callback
    //     console.log('error ' , error)
    // })
    database()
      .ref('/users/123')
      .on('value', snapshot => {
        console.log('User data: ', snapshot.val());
      });

    setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })
  }, [])

  const onSend = (messages = []) => {
    setState({
      
      messages: [...state.mes,GiftedChat.append(previousState.messages, messages)],
    })
  }

    return (
      <GiftedChat
        messages={state.messages}
        onSend={messages => onSend(messages)}
        user={{
          _id: 1,
        }}
      />
    )
}

export default Example;